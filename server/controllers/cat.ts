import  Cat from '../models/cat';
import BaseCtrl from './base';

class CatCtrl extends BaseCtrl {
  model = Cat;

  insertData = async (req, res) => {
    console.log(req.body,'controller');
    try {
      const obj = await new this.model({   ...req.body,}).save();
      res.status(201).json(obj);
    } catch (err) {
      return res.status(400).json({ error: err.message });
    }
  }
}

export default CatCtrl;
