import  Table from '../models/table';
import BaseCtrl from './base';

class TableCtrl extends BaseCtrl {
  model = Table;

  insertDataTable = async (req, res) => {
    console.log(req.body,'controller');
    try {
      const obj = await new this.model({   ...req.body,}).save();
      res.status(201).json(obj);
    } catch (err) {
      return res.status(400).json({ error: err.message });
    }
  }
}

export default TableCtrl;
