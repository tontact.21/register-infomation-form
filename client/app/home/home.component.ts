import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

import { UserService } from '../services/user.service';
import { ToastComponent } from '../shared/toast/toast.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  othertext = false;
  otheraddress = false;


  //////////// Address Select ////////////
  province: any = []
  sub_districts: any;
  provinces: any;
  idsub: any = [];
  zipcode: any;
  districts: any;
  id: any = [];
  //////////////////////////////////////////

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public toast: ToastComponent,
    private userService: UserService
  ) {}

  getdata() {
    this.userService.getProvince().subscribe(results => {
      this.provinces = results
      // console.log(this.provinces, 'Provinces');
    })
  }

  onChangProvice(provinceId) {

    // alert( this.provinces.filter(result=>{
    //   return result.province_code == provinceId.target.value
    // })[0].province)

    this.registerForm.patchValue({
      "province": this.provinces.filter(result => {
        return result.province_code == provinceId.target.value
      })[0].province
    })
    console.log("test", provinceId.target)
    this.userService.getDistrict(provinceId.target.value).subscribe(results => {
      this.districts = results
      // console.log("อำเภอ", this.districts);
    })
    // ... do other stuff here ...
  }

  onChangeDistrict(value) {

    this.id = value.split(",");
    this.registerForm.patchValue({
      "district": this.districts.filter(result => {
        return result.province_code == this.id[0] && result.amphoe_code == this.id[1]
      })[0].amphoe
    })
    this.userService.getSubDistrict(this.id[0], this.id[1]).subscribe(results => {
      this.sub_districts = results;
      // console.log("results", this.sub_districts);
    })
  }

  onChangeSubDistrict(value) {
    this.idsub = value.split(",");
    // console.log(this.idsub,'in')
    this.registerForm.patchValue({
      "sub_district": this.sub_districts.filter(result => {
        return result.province_code == this.idsub[0] && result.amphoe_code == this.idsub[1] && result.district_code == this.idsub[2]
      })[0].district
    })

    this.userService.getZipcode(this.idsub[0], this.idsub[1], this.idsub[2]).subscribe(results => {

      this.zipcode = results
      this.registerForm.patchValue({
        "zipcode": this.zipcode[0].zipcode
      })
      // console.log("dataAddress", this.zipcode)
    })
  }
  ngOnInit(): void {
    this.getdata();
    this.registerForm = this.formBuilder.group({
      Idcard: new FormControl('', [
        Validators.required,
        // Validators.minLength(2),
        // Validators.maxLength(13),
      ]),
      fullname : new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      age: new FormControl('', [Validators.required]),
      nationality: new FormControl('', [Validators.required]),
      pregnant:new FormControl('', [Validators.required]),
      pregnant_No: new FormControl(''),
      pregnant_Week: new FormControl(''),
      type_of_patient: new FormControl('', [Validators.required]),
      occupation: new FormControl('', [Validators.required]),
      workplace: new FormControl('', [Validators.required]),
      contact_No: new FormControl('', [Validators.required]),
      phone_app: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      houseNo:  new FormControl('', [Validators.required]),
      villageNo:  new FormControl('', [Validators.required]),
      village: new FormControl('', [Validators.required]),
      alley: new FormControl('', [Validators.required]),
      road: new FormControl('', [Validators.required]),
      province: new FormControl('', [Validators.required]),
      sub_district:  new FormControl('', [Validators.required]),
      district: new FormControl('', [Validators.required]),
      zipcode:  new FormControl('', [Validators.required]),
      congenital_disease:  new FormControl('', [Validators.required]),
      smoking:  new FormControl('', [Validators.required]),
      // email: new FormControl('', [
      //   Validators.email,
      //   Validators.required,
      //   Validators.minLength(3),
      //   Validators.maxLength(100),
      // ]),
      // password: new FormControl('', [
      //   Validators.required,
      //   Validators.minLength(6),
      // ]),
      // role: new FormControl('', [Validators.required]),
    });
      // for (let item of this.language) {
      //   if (item.check) {
      //     if(item.value != "อื่นๆ")
      //     this.registerservice.multilanguage(item, response.data).subscribe(response => {
      //       console.log(response)
      //     })
      //   }

      // }
  }
  get f() {
    return this.registerForm.controls;
  }

  
  otherType_of_Patient() {
    // alert(this.form.value.religion)
    if (this.registerForm.value.type_of_patient == "อื่นๆ") {
      // alert('in')
      this.othertext = true;
     
    } else {
      // alert('inin')
      this.othertext = false;
    }

  }
  otherAddress() {
    // alert(this.form.value.religion)
    if (this.registerForm.value.address == "อื่นๆ") {
      // alert('in')
      this.otheraddress = true;
     
    } else {
      // alert('inin')
      this.otheraddress = false;
    }

  }
  registerInformation(): void {
    console.log(  this.registerForm.value.pregnant,"test");
   
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    } else {
      this.userService.register(this.registerForm.value).subscribe(
        (res) => {
          this.toast.setMessage('you successfully registered!', 'success');
          this.router.navigate(['/']);
        },
        (error) => this.toast.setMessage('email already exists', 'danger')
      );
    }
  }
}
