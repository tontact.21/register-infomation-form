import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { CatService } from '../services/cat.service';
import { ToastComponent } from '../shared/toast/toast.component';
import { Cat } from '../shared/models/cat.model';

@Component({
  selector: 'app-add-cat-form',
  templateUrl: './add-cat-form.component.html',
  styleUrls: ['./add-cat-form.component.scss']
})

export class AddCatFormComponent implements OnInit {
  @Input() cats: Cat[];

  addCatForm: FormGroup;
  // name = new FormControl('', Validators.required);
  // age = new FormControl('', Validators.required);
  // weight = new FormControl('', Validators.required);

  constructor(private catService: CatService,
              private formBuilder: FormBuilder,
              public toast: ToastComponent) { }

  ngOnInit(): void {
    this.addCatForm = this.formBuilder.group({
      name:  new FormControl('', Validators.required),
      age:  new FormControl('', Validators.required),
      weight:  new FormControl('', Validators.required),
    });
  }

  addCat(): void {
    console.log(this.addCatForm.value,'catform');
    
    this.catService.addCat(this.addCatForm.value).subscribe(
      res => {
        console.log(res,'res');
        this.cats.push(res);
        this.addCatForm.reset();
        this.toast.setMessage('item added successfully.', 'success');
      },
      error => {
        console.log(error)
        this.toast.setMessage('item added unsuccess.', 'failed');
      }
      
    );
  }

}
