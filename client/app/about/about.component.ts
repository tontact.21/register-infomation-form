import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { AuthService } from '../services/auth.service';
import { ToastComponent } from '../shared/toast/toast.component';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent {
  registerForm: FormGroup;
  submitted = false;
  
  constructor( private formBuilder: FormBuilder,
    private router: Router,
    public toast: ToastComponent,
    private userService: UserService) { }


  ngOnInit(): void {
    // if (this.auth.loggedIn) {
    //   this.router.navigate(['/']);
    // }
    this.registerForm = this.formBuilder.group({
      ////// Vaccined History //////
      vaccine: new FormControl('', [Validators.required]),
      evidence: new FormControl('', [Validators.required]),

      first_dose: new FormControl(''),
      name_vaccine_1: new FormControl(''),
      place_first_dose: new FormControl(''),

      second_dose: new FormControl(''),
      name_vaccine_2: new FormControl(''),
      place_second_dose: new FormControl(''),

      third_dose: new FormControl(''),
      name_vaccine_3: new FormControl(''),
      place_third_dose: new FormControl(''),

      ////// Risk History //////
      go_aboad_cond: new FormControl('', [Validators.required]), // Yes Or No condition of go aboad. 
      country:  new FormControl('', [Validators.required]), 
      city:  new FormControl('', [Validators.required]), 
      arrival_thai: new FormControl('', [Validators.required]), 
      airline: new FormControl('', [Validators.required]), 
      flight_No: new FormControl('', [Validators.required]), 
      seat_No: new FormControl('', [Validators.required]), 

      case_1 :new FormControl('', [Validators.required]), 
      case_2 :new FormControl('', [Validators.required]), 
      case_3 :new FormControl('', [Validators.required]), 

      case_4 :new FormControl('', [Validators.required]), 
      case_5 :new FormControl('', [Validators.required]), 
      case_6 :new FormControl('', [Validators.required]), 
      case_7 :new FormControl('', [Validators.required]), 
      case_8 :new FormControl('', [Validators.required]), 
      case_9 :new FormControl('', [Validators.required]), 

      situation: new FormControl('', [Validators.required]),
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  registerInformation(): void {
    // this.auth.login(this.loginForm.value);
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    } else {
      this.userService.register(this.registerForm.value).subscribe(
        (res) => {
          this.toast.setMessage('you successfully registered!', 'success');
          this.router.navigate(['/about']);
        },
        (error) => this.toast.setMessage('email already exists', 'danger')
      );
    }
  
  }

}
